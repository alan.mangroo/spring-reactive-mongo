package org.mangroo.springreactivemongo;


import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mangroo.springreactivemongo.controller.ProductController;
import org.mangroo.springreactivemongo.dto.ProductDto;
import org.mangroo.springreactivemongo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@WebFluxTest(ProductController.class)
class SpringReactiveMongoApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ProductService service;

    @Test
    void addProductTest() {
        Mono<ProductDto> productDtoMono = Mono.just(new ProductDto("122", "mobile", 1, 2.34));
        when(service.saveProduct(productDtoMono)).thenReturn(productDtoMono);

        webTestClient.post().uri("/products").body(Mono.just(productDtoMono), ProductDto.class)
                .exchange()
                .expectStatus().isOk();
//                .expectBody()
//                .json("{}");
    }

    @Test
    void getProductsTest() {
        Flux<ProductDto> productDtoFlux = Flux.just(new ProductDto("122", "mobile", 1, 2.34), new ProductDto("300", "tv", 3, 5.34));
        when(service.getProducts()).thenReturn(productDtoFlux);

        Flux<ProductDto> responseBody = webTestClient.get().uri("/products")
                .exchange()
                .expectStatus().isOk()
                .returnResult(ProductDto.class)
                .getResponseBody();

        StepVerifier.create(responseBody)
                .expectSubscription()
                .expectNext(new ProductDto("122", "mobile", 1, 2.34))
                .expectNext(new ProductDto("300", "tv", 3, 5.34))
                .verifyComplete();
    }

    @Test
    void getProductTest() {
        Mono<ProductDto> productDtoMono = Mono.just(new ProductDto("122", "mobile", 1, 2.34));
        when(service.getproductById(any())).thenReturn(productDtoMono);

        Flux<ProductDto> responseBody = webTestClient.get().uri("/products/123")
                .exchange()
                .expectStatus().isOk()
                .returnResult(ProductDto.class)
                .getResponseBody();

        StepVerifier.create(responseBody)
                .expectSubscription()
                .expectNextMatches(p -> p.getName().equals("mobile"))
                .verifyComplete();
    }

    @Test
    void updateProductTest() {
        Mono<ProductDto> productDtoMono = Mono.just(new ProductDto("122", "mobile", 1, 2.34));
        when(service.updateProduct(productDtoMono, "123")).thenReturn(productDtoMono);

        webTestClient.put().uri("/products/update/123")
                .body(Mono.just(productDtoMono), ProductDto.class)
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void deleteProductTest() {
        when(service.deleteProduct(anyString())).thenReturn(Mono.empty());
        webTestClient.delete().uri("/products/delete/123")
                .exchange()
                .expectStatus().isOk();
    }
}
